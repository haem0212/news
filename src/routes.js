import React from 'react';
import AddCategoryPage from './pages/AdminPage/AddCategoryPage/AddCategoryPage';
import AddPostPage from './pages/AdminPage/AddPostPage/AddPostPage';
import CategoryPage from './pages/AdminPage/CategoryPage/CategoryPage';
import PostListPage from './pages/AdminPage/PostListPage/PostListPage';
import ContentCategoryPage from './pages/ContentCategoryPage/ContentCategoryPage';
import GeneralTopicPage from './pages/GeneralTopicPage/GeneralTopicPage';
import HomePage from './pages/HomePage/HomePage';
import NotFoundPage from './pages/NotFoundPage/NotFoundPage';
import PostContentPage from './pages/PostContentPage/PostContentPage';



const routes = [
    {
        path: '/',
        exact: true,
        main: () => <HomePage />
    },
    {
        path: '/post-list',
        exact: false,
        main: () => <PostListPage />
    },
    {
        path: '/post/add',
        exact: false,
        main: ({history}) => <AddPostPage history={history}/>
    },
    {
        path: '/post/:id/edit',
        exact: false,
        main: ({match, history}) => <AddPostPage match={match} history={history}/>
    },
    {
        path: '/category-list',
        exact: false,
        main: () => <CategoryPage />
    },
    {
        path: '/category/add',
        exact: false,
        main: ({history}) => <AddCategoryPage history={history}/>
    },
    {
        path: '/category/:id/edit',
        exact: false,
        main: ({match, history}) => <AddCategoryPage match={match} history={history}/>
    }
    ,
    {
        path: '/:title.:id',
        exact: false,
        main: ({match}) => <PostContentPage match={match}/>
    },
    {
        path: '/:name',
        exact: false,
        main: ({match}) => <ContentCategoryPage match={match}/>
    },
    {
        path: '/:nameGereral',
        exact: false,
        main: ({match}) => <GeneralTopicPage match={match}/>
    },
    {
        path: '',
        exact: false,
        main: () => <NotFoundPage />
    }
   
];

export default routes;