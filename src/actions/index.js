import * as Types from './../constants/ActionTypes';
import callApi from './../utils/apiCaller';

export const actFetchPostsRequest = () => {
    return dispatch => {
        return callApi('posts', 'GET', null).then(res => {
            dispatch(actFetchPosts(res.data));
        });
    };
}
export const actFetchPosts = (posts) => {
    return {
        type : Types.FETCH_POSTS,
        posts
    }
}
export const actDeletePostRequest = (id) => {
    return dispatch => {
        return callApi(`posts/${id}`, 'DELETE', null).then(res =>{
            dispatch(actDeletePost(id));
        })
    }
}

export const actDeletePost = (id) => {
    return {
        type : Types.DELETE_POST,
        id
    }
}

export const actAddPostRequest = (post) => {
    return dispatch => {
        return callApi('posts', 'POST', post).then(res => {
            dispatch(actAddPost(res.data));
        });
    }
}

export const actAddPost = (post) => {
    return {
        type : Types.ADD_POST,
        post
    }
}

export const actGetPostRequest = (id) => {
    return dispatch => {
        return callApi(`posts/${id}`, 'GET', null).then(res => {
            dispatch(actGetPost(res.data));
        });
    }
}

export const actGetPost = (post) => {
    return {
        type : Types.EDIT_POST,
        post
    }
}

export const actUpdatePostRequest = (post) => {
    return dispatch => {
        return callApi(`posts/${post.id}`, 'PUT', post).then(res => {
            dispatch(actUpdatePost(res.data));
        });
    }
}

export const actUpdatePost = (post) => {
    return {
        type : Types.UPDATE_POST,
        post
    }
}
//category
export const actFetchCategorysRequest = () => {
    return dispatch => {
        return callApi('categorys', 'GET', null).then(res => {
            dispatch(actFetchCategorys(res.data));
        });
    };
}
export const actFetchCategorys = (categorys) => {
    return {
        type : Types.FETCH_CATEGORYS,
        categorys
    }
}
export const actDeleteCategoryRequest = (id) => {
    return dispatch => {
        return callApi(`categorys/${id}`, 'DELETE', null).then(res =>{
            dispatch(actDeleteCategory(id));
        })
    }
}
export const actDeleteCategory = (id) => {
    return {
        type : Types.DELETE_CATEGORY,
        id
    }
}
export const actAddCategoryRequest = (category) => {
    return dispatch => {
        return callApi('categorys', 'POST', category).then(res => {
            dispatch(actAddCategory(res.data));
        });
    }
}
export const actAddCategory = (category) => {
    return {
        type : Types.ADD_CATEGORY,
        category
    }
}
export const actGetCategoryRequest = (id) => {
    return dispatch => {
        return callApi(`categorys/${id}`, 'GET', null).then(res => {
            dispatch(actGetCategory(res.data));
        });
    }
}
export const actGetCategory = (category) => {
    return {
        type : Types.EDIT_CATEGORY,
        category
    }
}
export const actUpdateCategoryRequest = (category) => {
    return dispatch => {
        return callApi(`categorys/${category.id}`, 'PUT', category).then(res => {
            dispatch(actUpdateCategory(res.data));
        });
    }
}
export const actUpdateCategory = (category) => {
    return {
        type : Types.UPDATE_CATEGORY,
        category
    }
}

