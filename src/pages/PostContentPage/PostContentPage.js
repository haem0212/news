import React, { Component } from 'react';
import Header from '../../components/User/Header/Header';
import Menu from '../../components/User/Menu/Menu';
import {Link} from 'react-router-dom'
import data from '../../db.json'
import LeftNav from '../../components/User/Contents/LeftNav/LeftNav';
import Sidebar from '../../components/User/Contents/Sidebar/Sidebar';
import toSlug from '../../constants/Slug';

class PostContentPage extends Component {
    render() {
        
        return (
            <div className="container pt-3">
                <Header/>
                <Menu/>
                <div className="row">
                    <div className="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <ul className="detail-menu">
                            {
                                data.categorys.map((value,key)=>{
                                    return(
                                        <LeftNav
                                            key ={key}
                                            image= {value.image}
                                            name={value.name}
                                        />
                                        )
                                    })
                                }
                        </ul>
                    </div>
                    {
                        data.posts.map((value,key)=>{
                            if(value.id === parseInt( this.props.match.params.id)){
                                return(
                                    <div className="col-6 postContent">
                                        <h3>{value.title}</h3>
                                        <div className="_breadcrumbs ">
                                            <span className="first _breadcrumb">
                                                <Link to="/">🏠</Link>
                                            </span>
                                            <span className="_breadcrumb">
                                                <Link to={toSlug(value.nameCate)}>{value.nameCate}</Link>
                                            </span>
                                                <p>{value.intro}</p>
                                                    <img src={value.image} class="img-fluid ${3|rounded-top,rounded-right,rounded-bottom,rounded-left,rounded-circle,|}"/>
                                        </div>
                                        
                                        <p>{value.content}</p>
                                   </div>
                                   )}
                            })

                    }
                    <div className="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <div className="sidebar">
                            <h4>Tiêu điểm</h4>
                            {      
                                data.posts.map((value,key)=>{
                                    return(
                                        <Sidebar
                                            key ={key}
                                            id = {value.id}
                                            title={value.title}
                                            image ={value.image}
                                        />
                                        )
                                    })
                            }
                            
                        </div>
                    </div>
               </div>
            </div>
        );
    }
}

export default PostContentPage;