
import React, { Component } from 'react';
import Header from '../../components/User/Header/Header';
import Menu from '../../components/User/Menu/Menu';
import {Link} from 'react-router-dom'
import data from '../../db.json'
import LeftNav from '../../components/User/Contents/LeftNav/LeftNav';
import Sidebar from '../../components/User/Contents/Sidebar/Sidebar';
import toSlug from '../../constants/Slug';
import News from '../../components/User/Contents/Posts/News/News';

class GeneralTopicPage extends Component {
    render() {
        console.log(this.props)
        return (
            <div className="container pt-3">
            <Header/>
            <Menu/>
            <div className="row">
                <div className="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                    <ul className="detail-menu">
                        {
                            data.categorys.map((value,key)=>{
                                return(
                                    <LeftNav
                                    key ={key}
                                    image = {value.image}
                                    name={value.name}
                                    />
                                    )
                                })
                            }
                    </ul>
                </div>
                
                     <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6 postContent">
                         {
                           data.generalTopic.map((value,key)=>{ 
                               console.log(toSlug(value.general))
                             if(value.general === ( this.props.match.params.general)){
                                 
                                return(
                                    <div className="_breadcrumbs">
                                        <span className="first _breadcrumb">
                                            <Link to="/">🏠</Link>
                                        </span>
                                        <span className="_breadcrumb">
                                            <Link to="/">{value.general}</Link>
                                        </span>
                                        <h3>{value.general}</h3>
                                    </div>
                                   )}
                                })
        
                            }
                    </div>
                        
                {/* <div     className="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                    <div className="sidebar">
                        <h4>Tiêu điểm</h4>
                        {      
                            data.posts.map((value,key)=>{
                                return(
                                    <Sidebar
                                        key ={key}
                                        id = {value.id}
                                        title={value.title}
                                        image ={value.image}
                                    />
                                )
                            })
                        }
                        
                    </div>
                </div> */}
           </div>
        </div>
        );
    }
}

export default GeneralTopicPage;
