import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { actAddCategoryRequest, actGetCategoryRequest, actUpdateCategoryRequest } from '../../../actions/index';
import { connect } from 'react-redux';
import data from './../../../db.json';
import LeftNav from '../../../components/Admin/LeftNav/LeftNav';
import Header from '../../../components/Admin/Header/Header';

class AddCategoryPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            id: '',
            image:'',
            txtName: '',  
            txtGeneralTopic:'', 
            chkbStatus: ''
        };
    }

    componentDidMount() {
        var { match } = this.props;
        if (match) {
            var id = match.params.id;
            this.props.onEditCategory(id);
        }
    }

    componentWillReceiveProps(nextProps){
        if(nextProps && nextProps.itemEditing){
            var {itemEditing} = nextProps;
            this.setState({
                id : itemEditing.id,
                txtName: itemEditing.name,
                image : itemEditing.image,
                txtGeneralTopic : itemEditing.generalTopic,
                chkbStatus : itemEditing.status
            });
        }
    }

    onChange = (e) => {
        var target = e.target;
        var name = target.name;
        var value = target.type === 'checkbox' ? target.checked : target.value;
        this.setState({
            [name]: value
        });
    }

    onSave = (e) => {
        e.preventDefault();
        var { id,txtName,image, txtGeneralTopic, chkbStatus } = this.state;
        var { history } = this.props;
        var category = {
            id : id,
            name: txtName,
            image:image,
            generalTopic: txtGeneralTopic,
            status : chkbStatus
        };
        if (id) {
            this.props.onUpdateCategory(category);
        } else {
            this.props.onAddCategory(category);
        }
        history.goBack();
    }

    render() {
        var {  txtName,image, txtGeneralTopic, chkbStatus} = this.state;
        var myStyle = {
            width :"500px"
        }
        return (
            <div >
            <LeftNav/> 
                <div id="right-panel" class="right-panel">
                    <Header/>
                <form onSubmit={this.onSave} style={myStyle}>
                    
                    <div className="form-group">
                        <label>Name</label>
                        <input
                            type="text"
                            className="form-control"
                            name="txtName"
                            value={txtName}
                            onChange={this.onChange}
                        />
                    </div>
                    <div className="form-group">
                        <label>Image</label>
                        <input
                            type="text"
                            className="form-control"
                            name="image"
                            value={image}
                            onChange={this.onChange}
                        />
                    </div>
                    <div className="form-group">
                        <label>General Topic</label>
                        <select 
                            name="txtGeneralTopic"
                            className="form-control selectpicker"
                            value ={txtGeneralTopic}
                            onChange={this.onChange}
                        >
                        <option>General Topic</option>   
                        {
                        data.generalTopic.map((value,key)=>{ 
                            return(
                                <option 
                                    key={key}
                                    value={value.name}
                                    >
                                {value.name}</option>
                            )
                            })
                        }
                      </select>
                    </div>
                    <div className="form-group">
                        <label>Trạng Thái: </label>
                    </div>
                    <div className="checkbox">
                        <label>
                            <input
                                type="checkbox"
                                name="chkbStatus"
                                value={chkbStatus}
                                onChange={this.onChange}
                                checked={chkbStatus}
                            />
                            Hiện
                        </label>
                    </div>
                    <Link to="/Category-list" className="btn btn-danger mr-10">
                        Trở Lại
                    </Link>
                    <button type="submit" className="btn btn-primary">Lưu Lại</button>
                </form>
               </div>         
            </div>
        );
    }

}

const mapStateToProps = state => {
    return {
        itemEditing : state.itemEditing
    }
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        onAddCategory : (category) => {
            dispatch(actAddCategoryRequest(category));
        },
        onEditCategory : (id) => {
            dispatch(actGetCategoryRequest(id));
        },
        onUpdateCategory : (category) => {
            dispatch(actUpdateCategoryRequest(category));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddCategoryPage);
