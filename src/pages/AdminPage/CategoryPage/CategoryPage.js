import React, { Component } from 'react';
import CategoryLists from './../../../components/Admin/Categorys/CategoryLists/CategoryLists';
import CategoryItem from './../../../components/Admin/Categorys/CategoryItem/CategoryItem';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { actFetchCategorysRequest, actDeleteCategoryRequest } from './../../../actions/index';
import LeftNav from '../../../components/Admin/LeftNav/LeftNav';
import Header from '../../../components/Admin/Header/Header';


class CategoryListPage extends Component {

    componentDidMount() {
        this.props.fetchAllCategorys();
    }

    onDelete = (id) => {
        this.props.onDeleteCategory(id);
    }

    render() {
        var { categorys } = this.props;
        
        return (
            <div >
            <LeftNav/> 
                <div id="right-panel" class="right-panel">
                    <Header/>   
                    <CategoryLists>
                        {this.showCategorys(categorys)}
                    </CategoryLists>
                </div>
            </div>
        );
    }

    showCategorys(categorys) {
        var result = null;
        if (categorys.length > 0) {
            result = categorys.map((category, index) => {
                return (
                    <CategoryItem
                        key={index}
                        category={category}
                        index={index}
                        onDelete={this.onDelete}
                    />
                );
            });
        }
        return result;
    }

}

const mapStateToProps = state => {
    return {
        categorys: state.categorys
    }
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        fetchAllCategorys : () => {
            dispatch(actFetchCategorysRequest());
        },
        onDeleteCategory : (id) => {
            dispatch(actDeleteCategoryRequest(id));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CategoryListPage);
