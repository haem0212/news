import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { actAddPostRequest, actGetPostRequest, actUpdatePostRequest } from '../../../actions/index';
import { connect } from 'react-redux';
import data from './../../../db.json'
import LeftNav from '../../../components/Admin/LeftNav/LeftNav';
import Header from '../../../components/Admin/Header/Header';

class AddPostPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            id: '',
            image:'',
            nameCate:'',
            txtTitle: '',
            txtIntro: '',
            txtContent:'',
            chkbStatus: ''
        };
    }

  
    
    componentDidMount() {
        var { match } = this.props;
        if (match) {
            var id = match.params.id;
            this.props.onEditPost(id);
        }
    }

    componentWillReceiveProps(nextProps){
        if(nextProps && nextProps.itemEditing){
            var {itemEditing} = nextProps;
            this.setState({
                id : itemEditing.id,
                txtTitle : itemEditing.title,
                image:itemEditing.image,    
                txtNameCate:itemEditing.nameCate,
                txtIntro : itemEditing.intro,
                txtContent : itemEditing.content,
                chkbStatus : itemEditing.status

            });
        }
    }

    onChange = (e) => {
        var target = e.target;
        var name = target.name;
        var value = target.type === 'checkbox' ? target.checked : target.value;
        this.setState({
            [name]: value
        });
    }

    onSave = (e) => {
        e.preventDefault();
        var { id ,txtNameCate,txtTitle,image,txtIntro,txtContent, chkbStatus } = this.state;
        var { history } = this.props;
        var post = {
            id : id,
            title : txtTitle,
            image:image,
            intro : txtIntro,
            nameCate:txtNameCate,
            content:txtContent,
            status : chkbStatus
           
        };
        if (id) {
            this.props.onUpdatePost(post);
        } else {
            this.props.onAddPost(post);
        }
        history.goBack();
    }

    render() {
        var {  txtNameCate,txtTitle,image,txtIntro, txtContent, chkbStatus} = this.state;
        var myStyle = {
            width :"500px"
        }
        return (
            <div >
            <LeftNav/> 
                <div id="right-panel" class="right-panel">
                    <Header/>
                <form onSubmit={this.onSave} style={myStyle}>
                    <div className="form-group">
                        <label>Category</label>
                        <br/>
                        <select 
                            name="txtNameCate"
                            className="form-control selectpicker"
                            value ={txtNameCate}
                            onChange={this.onChange}
                        >
                        <option>Category</option>   
                        {
                        data.categorys.map((value,key)=>{ 
                            return(
                                <option 
                                    key={key}
                                    value={value.name}
                                    >
                                {value.name}</option>
                            )
                            })
                        }
                      </select>
                    </div>
                    <div className="form-group">
                        <label>Title</label>
                        <input
                            type="text"
                            className="form-control"
                            name="txtTitle"
                            value={txtTitle}
                            onChange={this.onChange}
                        />
                    </div>
                    <div className="form-group">
                        <label>Intro: </label>
                        <input
                            type="text"
                            className="form-control"
                            name="txtIntro"
                            value={txtIntro}
                            onChange={this.onChange}
                        />
                    </div>
                    <div className="form-group">
                        <label>Image</label>
                        <input 
                        type="text"
                        className="form-control"
                        name="image"
                        value={image}
                        onChange={this.onChange}
                        />
                    </div>
                   
                    <div className="form-group">
                        <label>Content: </label>
                        
                        <textarea 
                            id="summernote"
                            className="form-control"
                            name="txtContent"
                            value={txtContent}
                            onChange={this.onChange}/>
                    </div>
                    
                    <div className="form-group">
                        <label>Trạng Thái: </label>
                    </div>
                    <div className="checkbox">
                        <label>
                            <input
                                type="checkbox"
                                name="chkbStatus"
                                value={chkbStatus}
                                onChange={this.onChange}
                                checked={chkbStatus}
                            />
                            Hiện
                        </label>
                    </div>
                    <Link to="/post-list" className="btn btn-danger mr-10">
                        Trở Lại
                    </Link>
                    <button type="submit" className="btn btn-primary">Lưu Lại</button>
                </form>
                
            </div>
            </div>
        );
    }

}

const mapStateToProps = state => {
    return {
        itemEditing : state.itemEditing
    }
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        onAddPost : (post) => {
            dispatch(actAddPostRequest(post));
        },
        onEditPost : (id) => {
            dispatch(actGetPostRequest(id));
        },
        onUpdatePost : (post) => {
            dispatch(actUpdatePostRequest(post));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddPostPage);
