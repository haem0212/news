import React, { Component } from 'react';

import { connect } from 'react-redux';

import PostItem from '../../../components/Admin/Posts/PostItem/PostItem';
import PostList from '../../../components/Admin/Posts/PostList/PostList';
import { actFetchPostsRequest, actDeletePostRequest } from './../../../actions/index';
import LeftNav from '../../../components/Admin/LeftNav/LeftNav';
import Header from '../../../components/Admin/Header/Header';

class PostListPage extends Component {

    componentDidMount() {
        this.props.fetchAllPosts();
    }

    onDelete = (id) => {
        this.props.onDeletePost(id);
    }

    render() {
        var { posts } = this.props;
        
        return (
            <div >
            <LeftNav/> 
                <div id="right-panel" class="right-panel">
                    <Header/>   
                    <PostList>
                    {this.showPosts(posts)}
                </PostList>
                </div>
            </div>
            
        );
    }

    showPosts(posts) {
        var result = null;
        if (posts.length > 0) {
            result = posts.map((post, index) => {
                return (
                    <PostItem
                        key={index}
                        post={post}
                        index={index}
                        onDelete={this.onDelete}
                    />
                );
            });
        }
        return result;
    }

}

const mapStateToProps = state => {
    return {
        posts: state.posts
    }
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        fetchAllPosts : () => {
            dispatch(actFetchPostsRequest());
        },
        onDeletePost : (id) => {
            dispatch(actDeletePostRequest(id));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(PostListPage);
