import React, { Component } from 'react';
import Header from '../../components/User/Header/Header';
import Contents from '../../components/User/Contents/Content';
import Menu from '../../components/User/Menu/Menu';

class HomePage extends Component {
  
    render() {
      
        return (
            <div className="container pt-3">
                <Header/>
                <Menu/>
                <Contents/>
            </div>
        );
    }
}

export default (HomePage);

