import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class LeftNav extends Component {
    render() {
        return (
            <aside id="left-panel" className="left-panel">
            <nav className="navbar navbar-expand-sm navbar-default">
            <div id="main-menu" className="main-menu collapse navbar-collapse">
                <ul className="nav navbar-nav">
                <li>
                    <a href="index.html"><i className="menu-icon fa fa-laptop" />Dashboard </a>
                </li>
                <li className="menu-item-has-children active dropdown">
                    <Link to="/post-list" className="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i className="menu-icon fa fa-table" />Tables</Link>
                    <ul className="sub-menu children dropdown-menu">
                    <li><i className="fa fa-table" /><Link to="post-list">Post</Link></li>
                    <li><i className="fa fa-table" /><Link to="category-list">Categoryh</Link></li>
                    </ul>
                </li>
                </ul>
            </div>{/* /.navbar-collapse */}
            </nav>
        </aside>
        );
    }
}

export default LeftNav;