import React, { Component } from 'react';
import { Link } from 'react-router-dom';

      



class PostItem extends Component {

    onDelete = (id) => {
        if (confirm('Bạn chắc chắn muốn xóa ?')) { //eslint-disable-line
            this.props.onDelete(id);
        }
    }

    render() {
        var { post, index } = this.props;
        var statusName = post.status ? 'Hiện' : 'Ẩn';
        var statusClass = post.status ? 'warning' : 'default';
        // var img = require(`${process.env.PUBLIC_URL}/assets/img/aaa.jpg`);
        // console.log(img)
       var myStyle = {
           height:"70px",
           width : "100px"
       }
        return (
            
            <tr>
                <td>{index + 1}</td>
                <td>{post.nameCate}</td>
                <td>{post.title}</td>
                <td>
                    <img src={post.image} style={myStyle}/>
                </td>
                <td>{post.intro}</td>
                <td>
                    <span className={`label label-${statusClass}`}>
                        {statusName}
                    </span>
                </td>
                <td>
                    <Link
                        to={`/post/${post.id}/edit`}
                        className="btn btn-success mr-10"
                    >
                        Sửa
                    </Link>
                    <button
                        type="button"
                        className="btn btn-danger"
                        onClick={() => this.onDelete(post.id)}
                    >
                        Xóa
                    </button>
                </td>
            </tr>
        );
    }
}

export default PostItem;
