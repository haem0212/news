import React, { Component } from 'react';
import { Link } from 'react-router-dom';


class PostList extends Component {
    render() {
        return (
            <div className="content">
            <div className="animated fadeIn">
              <div className="row">
                <div className="col-md-12">
                  <div className="card">
                    <div className="card-header">
                      <strong className="card-title">Post List</strong>

                    <Link to="/post/add" className="btn btn-danger ml-5">Add Post</Link>
                    </div>
                    <div className="card-body">
                      <table id="bootstrap-data-table" className="table table-striped table-bordered">
                      <thead>
                            <tr>
                                <th>STT</th>
                                <th>Category</th>
                                <th>Title</th>
                                <th>Image</th>
                                <th>Intro</th>     
                                <th>Trạng Thái</th>
                                <th>Hành Động</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.props.children}
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>{/* .animated */}
          </div>
        );
    }
}

export default PostList;
