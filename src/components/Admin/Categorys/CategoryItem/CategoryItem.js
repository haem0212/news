import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class CategoryItem extends Component {

    onDelete = (id) => {
        if (confirm('Bạn chắc chắn muốn xóa ?')) { //eslint-disable-line
            this.props.onDelete(id);
        }
    }

    render() {
        var { category, index } = this.props;
        var statusName = category.status ? 'Hiện' : 'Ẩn';
        var statusClass = category.status ? 'warning' : 'default';
        return (
            <tr>
                <td>{index + 1}</td>
                <td>{category.name}</td>        
                <td>
                    <span className={`label label-${statusClass}`}>
                        {statusName}
                    </span>
                </td>
                <td>
                    <Link
                        to={`/category/${category.id}/edit`}
                        className="btn btn-success mr-10"
                    >
                        Sửa
                    </Link>
                    <button
                        type="button"
                        className="btn btn-danger"
                        onClick={() => this.onDelete(category.id)}
                    >
                        Xóa
                    </button>
                </td>
            </tr>
        );
    }
}

export default CategoryItem;
