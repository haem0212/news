import React from 'react'
import {Link} from 'react-router-dom'
function Logo() {
    return (
        <div className="col-4">
            <Link to="/">KhoasHoc.Tv</Link>
        </div>
    )
}

export default Logo
