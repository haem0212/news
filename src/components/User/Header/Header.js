import React from 'react'
import Logo from './Logo/Logo'
import SearchBox from './SearchBox/SearchBox'

function Header() {
    return (
        <section id="header mt-3">
        <div className="row clearfix mt-3">
            <Logo/>
            <SearchBox/>
        </div>
      </section>
    )
}

export default Header
