import React from 'react'

function SearchBox() {
    return (
        <div className="col-4">
            <div className="input-group mb-3">
            <input type="text" className="form-control" placeholder="Tìm kiếm" aria-label="Recipient's username" aria-describedby="basic-addon2" />
            <div className="input-group-append">       
                <a href="#" className="input-group-text" id="basic-addon2">Tìm kiếm</a>
            </div>
            </div>
        </div>
    )
}

export default SearchBox
