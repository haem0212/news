import React, { Component } from 'react';
import { Route, Link } from 'react-router-dom';
import toSlug from '../../../constants/Slug';
import data from '../../../db.json'

class Menu extends Component {
    render() {
        // var active = match ? 'nav-item active' : 'nav-item';
        
        return (
            <div className=" menu row mb-3">
                <ul className="nav">
                    {
                            data.generalTopic.map((value,key)=>{
                                if(value.id === 0){
                                    return(
                                        <li className="nav-item">
                                            <Link to="/" className="link">
                                                Trang chủ
                                            </Link>
                                        </li>
                                    )
                                }else{
                                return(
                                    <li className="nav-item">
                                        <Link to={toSlug(value.general)} className="link">
                                            {value.general}
                                        </Link>
                                    </li>
                                )
                                }
                            })
                        }
                </ul>
            </div>
        );
    }

    
}
export default Menu;
