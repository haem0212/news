import React from 'react'
import LeftNav from './LeftNav/LeftNav'
import Posts from './Posts/Posts'
import Sidebar from './Sidebar/Sidebar'
import data from '../../../db.json'

function Content() {
    return (
        <section id="content" className="pt-3"> 
           <div className="row">
                <div className="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                    <ul className="detail-menu">
                    {
                            data.categorys.map((value,key)=>{
                                return(
                                    <LeftNav
                                        key ={key}
                                        image = {value.image}
                                        name={value.name}
                                    />
                                    )
                                })
                        }
                    </ul>
                </div>
               <Posts/>
               <div className="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                    <div className="sidebar">
                        <h4>Tiêu điểm</h4>
                  {      
                data.posts.map((value,key)=>{
                        return(
                            <Sidebar
                                key ={key}
                                id = {value.id}
                                 title={value.title}
                                 image ={value.image}
                            />
                            )
                        })
                 }
                        
                    </div>
                </div>
                            
            </div> 
        </section>
    )
}

export default Content
