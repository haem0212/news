import React, { Component } from 'react';
import {Link} from 'react-router-dom'
import toSlug from '../../../../constants/Slug';

class LeftNav extends Component {
    render() {
        return (
         <li className="nav-item">
             <Link to={toSlug(this.props.name)}>
                 <img className ="img-menu" src={this.props.image}/>
                 {this.props.name}
             </Link>
        </li>
        );
    }
}

export default LeftNav;