import React, { Component } from 'react';
import {Link} from 'react-router-dom'
import toSlug from '../../../../constants/Slug'
class Sidebar extends Component {
    render() {
        const styleImg = {
            height: "70px",
            width:"90px"
        }
        return (
           
        <ul className="nav flex-column ">
            <li className="nav-item row">
                <img src={this.props.image} style={styleImg} className="col-4 pb-2" />
                <Link to={toSlug(this.props.title)+"." + this.props.id} className="col-8">{this.props.title}</Link>
            </li>
    </ul>   
        );
    }
}

export default Sidebar;