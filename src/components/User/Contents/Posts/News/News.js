
import React, { Component } from 'react'
import {Link} from 'react-router-dom'

import toSlug from '../../../../../constants/Slug'



export default class News extends Component {
    render() {
        return (
            <div className="posts row mt-3">
                    <ul className="nav flex-column col-12 ">             
                        <li className="nav-item row mb-1" >
                                <img src={this.props.image} className="col-4" />
                                <div className="col-8">
                                    <Link to={toSlug(this.props.title)+"." + this.props.id} className="col-8 h5">{this.props.title}</Link>
                                    <p>{this.props.intro}</p>
                                </div>
                        </li>
            
                    </ul>
                </div>
                   
                
          
        )
    }
}

