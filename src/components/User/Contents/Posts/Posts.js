import React, { Component } from 'react';
import News from './News/News';
import Slider from './Slider/Slider';
import data from '../../../../db.json'

class Posts extends Component {
    
    render() {
        return (
            <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">   
                <Slider/>
            {  data.posts.map((value,key)=>{
                        return(
                        <News
                            key ={key}
                            id = {value.id}
                            image = {value.image}
                            title={value.title}
                            intro={value.intro}
                        />
                        )
                })
            }       
            </div>
        );
    }
}
export default Posts;