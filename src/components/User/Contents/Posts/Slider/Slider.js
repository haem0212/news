import React, { Component } from 'react';
import data from '../../../../../db.json'

class Slider extends Component {
    render() {
       const myStyle = {
           height:"300px",
           width:"100%"
       }
            return (    
              <div id="carouselExampleIndicators" className="carousel slide" data-ride="carousel">
                <ol className="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to={1} className="active" />
                    <li data-target="#carouselExampleIndicators" data-slide-to={2} />
                    <li data-target="#carouselExampleIndicators" data-slide-to={3} />
                </ol>
                <div className="carousel-inner">
                    {
                        data.posts.map((value)=>{
                            const carouselClass = parseInt(value.id) === 1? 'carousel-item active':'carousel-item '
                            console.log(carouselClass)
                            return(
                                <div className={carouselClass}>
                                    <img className="d-block w-100" src={value.image} alt="First slide" style={myStyle} />
                                    <div className="carousel-caption d-none d-md-block">     
                                        <p>{value.title} </p>
                                    </div>
                                </div>
                            )
                        })
                    }
                </div>
                <a className="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span className="carousel-control-prev-icon" aria-hidden="true" />
                    <span className="sr-only">Previous</span>
                </a>
                <a className="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span className="carousel-control-next-icon" aria-hidden="true" />
                    <span className="sr-only">Next</span>
                </a>
                </div>
        );
    }
}

export default Slider;