import { combineReducers } from 'redux';
import posts from './posts';
import categorys from './categorys';
import itemEditing from './itemEditing';

const appReducers = combineReducers({
    posts,
    categorys,
    itemEditing
});

export default appReducers;