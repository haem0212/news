import * as Types from './../constants/ActionTypes';

var initialState = {};

const itemEditing = (state = initialState, action) => {
    switch(action.type){
        case Types.EDIT_POST:
            return action.post;
        case Types.EDIT_CATEGORY:
            return action.category;    
        default:
            return state;
    }
}

export default itemEditing;
